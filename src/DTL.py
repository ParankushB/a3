
import pandas as pd 
import numpy as np 
import scipy as sc
import scipy.stats
import math
from anytree import AnyNode, Node, RenderTree, find_by_attr
from anytree.exporter import DotExporter

#produces the database from local .txt file
#produces the database from local .txt file
def database():
    
    df = pd.read_csv('mushrooms.csv')
    #print(df)
    
    return df 


#Implements the main DTL algorithm.
# The class should somehow be encoded in the datapoints
#  structure. A reasonable approach would be to use
#  the feature name "class" to refer to it consistently.
def DTL(datapoints, features, default_value):

    #print('features: ', features)

    #tree = TreeNode(0, 0)
    tree = Node(0)

    if datapoints.empty:
        return default_value

    elif Compute_Mode(datapoints) == len(datapoints):
        return Return_Mode(datapoints)

    elif not features:
        return Return_Mode(datapoints)

    else:
        bestFeature = Choose_Attribute(features, datapoints)
        
        best = Make_Subsets(datapoints, bestFeature)

        #tree = {bestFeature:{}}
        tree = Node(bestFeature)

        #tree.setAttribute(bestFeature)

        for subset in best:

            if bestFeature in features:
                features.remove(bestFeature)

            subtree = DTL(subset, features, Return_Mode(subset))

            print(subtree)
            
            if type(subtree) is str:
                #tree.addChild(subtree)
                #print(subtree)
                print()
            else:
                #tree.children.append(subtree)

                #featureIndex = fullFeatureList.index(bestFeature)
                #subtree.setValue(subset.iloc[0, featureIndex])
                #print(tree)
                print()
                
                #tree.addChild(subtree)

            #adding to tree incorrectly [find out how to properly add to the dictionary]
            #tree[bestFeature][subset] = subtree
                
    return tree

#Returns the feature with the greatest information
# gain for the provided datapoints.
def Choose_Attribute(features, datapoints):

    bestInformationGain = -1

    bestFeature = ''

    for feature in features:  

        subsets = Make_Subsets(datapoints, feature)

        entropy = Compute_Entropy(subsets, feature)
        
        currentInformationGain = Compute_InformationGain(entropy, datapoints)
        
        if currentInformationGain > bestInformationGain:
            bestInformationGain = currentInformationGain
            bestFeature = feature
    
    return bestFeature


#Returns a set of subsets of datapoints
# such that all points in a subset have
# the same value of the indicated feature.
def Make_Subsets(datapoints, feature):
    
    #list of diff attributes
    uniqueAttributes = []
    
    #create a array of arrays, the number of arrays = number of unique features in the column
    subsets = []
    
    #first see how many unqiue values in the feature (col) there are
    for value in datapoints[feature]:
        if value not in uniqueAttributes:
            uniqueAttributes.append(value)
    
    #initalize the size of subsets
    for attribute in uniqueAttributes:
        subsets.append([])
    
    #set each subset to coorespond with its unique attribute
    for index, row in datapoints.iterrows():
        for attribute in uniqueAttributes:
            if attribute == datapoints.loc[index, feature]:
                currentIndex = uniqueAttributes.index(attribute)
                subsets[currentIndex].append(row)


    dfsubsets = []

    #dataframe=pd.DataFrame(e, columns=['a']) 
    for subset in subsets:
        df = pd.DataFrame(subset, columns=fullFeatureList)
        dfsubsets.append(df)
    
    return dfsubsets




#Computes the mode of the class of datapoints.
def Compute_Mode(datapoints):

    #list of diff class values
    uniqueClassValues = []

    #first see how many unqiue values in the feature (col) there are
    for value in datapoints['class']:
        if value not in uniqueClassValues:
            uniqueClassValues.append(value)

    #list of class counts
    listOfClassCounts = []

    #initalize the size for list of class counts
    for value in uniqueClassValues:
        listOfClassCounts.append(0)

    for index, row in datapoints.iterrows():
        for value in uniqueClassValues:
            if value == row['class']:
                currentIndex = uniqueClassValues.index(value)
                listOfClassCounts[currentIndex] += 1

    mode = 0
    for item in listOfClassCounts:
        if item > mode:
            mode = item
    
    return mode 


#Computes the mode of the class of datapoints.
def Return_Mode(datapoints):

    #list of diff class values
    uniqueClassValues = []

    #first see how many unqiue values in the feature (col) there are
    for value in datapoints['class']:
        if value not in uniqueClassValues:
            uniqueClassValues.append(value)

    #list of class counts
    listOfClassCounts = []

    #initalize the size for list of class counts
    for value in uniqueClassValues:
        listOfClassCounts.append(0)

    for index, row in datapoints.iterrows():
        for value in uniqueClassValues:
            if value == row['class']:
                currentIndex = uniqueClassValues.index(value)
                listOfClassCounts[currentIndex] += 1

    mode = 0
    itemIndex = 0
    for index, item in enumerate(listOfClassCounts):
        if item > mode:
            mode = item
            itemIndex = index
    
    return uniqueClassValues[itemIndex]

def Compute_InformationGain(entropy, datapoints):

    #list of diff class values
    uniqueClassValues = []

    #first see how many unqiue values in the feature (col) there are
    for value in datapoints['class']:
        if value not in uniqueClassValues:
            uniqueClassValues.append(value)

    #list of class counts
    listOfClassCounts = []

    #initalize the size for list of class counts
    for value in uniqueClassValues:
        listOfClassCounts.append(0)

    for index, row in datapoints.iterrows():
        for value in uniqueClassValues:
            if value == row['class']:
                currentIndex = uniqueClassValues.index(value)
                listOfClassCounts[currentIndex] += 1


    sum = 0
    for value in listOfClassCounts:
        sum = sum + value 

    average = []
    for value in listOfClassCounts:
        average.append(value/sum)

    
    RunE = 0

    #-p(edible) * log(p(edible)) - p(pos) * log(p(pos))
    for value in average:

        #get log
        val = 0
        if value != 0:
            val = value * math.log(value, 2)
        RunE = RunE - val

    informationGain = RunE - entropy

    return informationGain

#Computes the information entropy of datapoints w.r.t
# the class.
def Compute_Entropy(subsets, classColumn):

    classColumnIndex = fullFeatureList.index(classColumn)

    #list of entropies
    entropyList = []
    
    #list of diff classes
    uniqueClasses = []

    #represents the number of times a value has shown up
    ClassesCount = []
    
    #first see how many unique values in each classcolumn there are
    for subset in subsets:
        for index, row in subset.iterrows():
            if row[classColumnIndex] not in uniqueClasses:
                uniqueClasses.append(row[classColumnIndex])
                ClassesCount.append(0) #set 0 for each element of the array.
    

    duplicateClassCount = ClassesCount.copy()

    #-p(edible) * log(p(edible)) - p(pos) * log(p(pos))

    for subset in subsets:
        for index, row in subset.iterrows():
            for value in uniqueClasses:
                if row[classColumnIndex] == value:
                    currentIndex = uniqueClasses.index(value)
                    duplicateClassCount[currentIndex] = duplicateClassCount[currentIndex] + 1

        RunE = 0

        #find probability of each class
        for item in duplicateClassCount:
            item = item/len(subset)

            #get log
            val = 0
            if item != 0:
                val = item * math.log(item, 2)
            RunE = RunE - val

        #reset count per subset
        duplicateClassCount = ClassesCount.copy()
        entropyList.append(RunE)


    #get average of all entropies
    sumOfEntropies = 0
    for entropy in entropyList:
        sumOfEntropies += entropy
    average = sumOfEntropies/ len(entropyList)
    
    return average



def printTree(treeNode):

    print(treeNode.attribute)
    print(treeNode.value)

    for child in treeNode.Children:

        if type(child) is TreeNode:
            printTree(child)
        else:
            print(child)


class TreeNode:        

    attribute = ''

    value = ''
    Children = []

    def __init__(self, attribute, value):
        self.attribute = attribute
        self.value = value

    def addChild(self, node):
        self.Children.append(node)
    
    def setAttribute(self, attribute):
        self.attribute = attribute

    def setValue(self, value):
        self.value = value




df = database()

fullFeatureList = list(df)


#subsets = Make_Subsets(df, 'cap-shape')
#print(subsets)

#entropy = Compute_Entropy(subsets, 0)
#print(entropy)

#Compute_InformationGain(entropy, df)

#print(Return_Mode(df))

headers = list(df)
headers.pop(0)

tree = DTL(df, headers, 'class')

#printTree(tree)

#bestFeature = Choose_Attribute(headers, df)

#print(bestFeature)