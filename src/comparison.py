from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.preprocessing import OneHotEncoder
import sklearn.decomposition
import sklearn.preprocessing
from sklearn import svm, tree
import pandas as pd 
import numpy as np 
import scipy as sc
import scipy.stats
import math
from sklearn_pandas import DataFrameMapper, cross_val_score
from sklearn.pipeline import Pipeline
from mlxtend.plotting import plot_decision_regions
import matplotlib.pyplot as plt

#produces the database from local .txt file
def database():
    
    df = pd.read_csv('train.csv')
    #print(df)
    
    return df 



database = database()

y = database.pop('class').values
vc = database['cap-shape'].value_counts()

X = database[['cap-shape', 'cap-surface', 'cap-color', 'bruises', 'odor', 'gill-attachment', 'gill-spacing', 'gill-size', 'gill-color', 'stalk-shape', 'stalk-root', 'stalk-surface-above-ring', 'stalk-surface-below-ring', 'stalk-color-above-ring', 'stalk-color-below-ring', 'veil-type', 'veil-color', 'ring-number', 'ring-type', 'spore-print-color', 'population', 'habitat']]

le = sklearn.preprocessing.LabelEncoder()
X = X.apply(le.fit_transform)

clf = svm.SVC(gamma = 'auto')
clf.fit(X, y)

test = pd.read_csv('test.csv')
testX = test[['cap-shape', 'cap-surface', 'cap-color', 'bruises', 'odor', 'gill-attachment', 'gill-spacing', 'gill-size', 'gill-color', 'stalk-shape', 'stalk-root', 'stalk-surface-above-ring',
                  'stalk-surface-below-ring', 'stalk-color-above-ring', 'stalk-color-below-ring', 'veil-type', 'veil-color', 'ring-number', 'ring-type', 'spore-print-color', 'population', 'habitat']]

testX = testX.apply(le.fit_transform)
testY = test.pop('class').values

print(clf.score(testX, testY))

id3 = tree.DecisionTreeClassifier(criterion="entropy")
id3.fit(X, y)

print(id3.score(testX, testY))


#neigh = KNeighborsClassifier(n_neighbors=3)
#neigh.fit(X, y)
